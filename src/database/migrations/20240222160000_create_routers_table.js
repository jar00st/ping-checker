
exports.up = function(knex) {
    return knex.schema.createTable('routers', function(table) {
        table.increments('id').primary();
        table.string('ip').notNullable();
        table.string('hostname').notNullable();
        table.string('name').notNullable();
        table.string('identifier').notNullable().unique();
        table.timestamps(true, true);
    });
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists('routers');
};