exports.seed = function(knex) {
    return knex('routers').del()
        .then(function () {
            const routersData = [];
            for (let i = 1; i <= 25; i++) {
                routersData.push({
                    ip: `192.168.1.${i}`,
                    hostname: `router${i}`,
                    name: `Router ${i}`,
                    identifier: `r${i}`
                });
            }
            return knex('routers').insert(routersData);
        });
};
