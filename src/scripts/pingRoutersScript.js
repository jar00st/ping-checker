const cron = require('node-cron');
const ping = require('net-ping');
const Router = require('../models/routers');
const redisClient = require('../config/redis'); // Asegúrate de que esta ruta sea correcta

let session = ping.createSession();

const realizarPingATodosLosRouters = async () => {
    console.log('Iniciando tarea de ping');

    try {
        const routers = await Router.query();

        routers.forEach(router => {
            session.pingHost(router.ip, (error, target, sent, rcvd) => {
                const ms = error ? 0 : rcvd - sent;
                const paquetesPerdidos = error ? 1 : 0;
                const timestamp = Date.now();
                const result = { latencia: ms, paquetesPerdidos, timestamp };

                redisClient.hset(`ping:${router.id}`, timestamp.toString(), JSON.stringify(result), (err) => {
                    if (err) {
                        console.error('Error al guardar el resultado de ping en Redis:', err);
                    }
                });
                
                redisClient.expire(`ping:${router.id}`, 86400);

                if (error) {
                    console.error(`${target}: No se pudo alcanzar el host`);
                } else {
                    console.log(`${target}: Host alcanzado con una latencia de ${ms}ms`);
                }
            });
        });
    } catch (error) {
        console.error('Error al obtener la lista de routers:', error);
    }
};


module.exports = realizarPingATodosLosRouters;
