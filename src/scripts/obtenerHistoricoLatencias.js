const redisClient = require('../config/redis');

const obtenerHistoricoLatencias = async (routerId) => {
    const latencias = [];

    try {
        const pings = await new Promise((resolve, reject) => {
            redisClient.hgetall(`ping:${routerId}`, (err, result) => {
                if (err) reject(err);
                resolve(result);
            });
        });

        if (pings) {

            for (const [timestamp, value] of Object.entries(pings)) {
                latencias.push({ timestamp: parseInt(timestamp), ...JSON.parse(value) });
            }
            latencias.sort((a, b) => a.timestamp - b.timestamp);
        }
    } catch (error) {
        console.error('Error al obtener las latencias:', error);
        throw error;
    }

    return latencias;
};

module.exports = obtenerHistoricoLatencias;