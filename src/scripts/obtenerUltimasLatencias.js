const Router = require('../models/routers');
const redisClient = require('../config/redis');

const obtenerUltimasLatencias = async () => {
    const latencias = {};

    try {
        const routers = await Router.query();

        for (const router of routers) {
            const pings = await new Promise((resolve, reject) => {
                redisClient.hgetall(`ping:${router.id}`, (err, result) => {
                    if (err) reject(err);
                    resolve(result);
                });
            });

            if (pings) {

                const ultimaLatencia = Object.keys(pings).sort((a, b) => b - a)[0];
                latencias[router.id] = JSON.parse(pings[ultimaLatencia]);
            } else {
                latencias[router.id] = { latencia: 'No disponible' };
            }
        }
    } catch (error) {
        console.error('Error al obtener las latencias:', error);
        throw error;
    }

    return latencias;
};

module.exports = obtenerUltimasLatencias;
