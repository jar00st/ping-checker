const cron = require('node-cron');
const realizarPingATodosLosRouters = require('../scripts/pingRoutersScript');


cron.schedule('*/30 * * * *', async () => {
    await realizarPingATodosLosRouters();
});
