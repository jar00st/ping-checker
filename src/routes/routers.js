const express = require('express');
const router = express.Router();
const Router = require('../models/routers');
const realizarPingATodosLosRouters = require('../scripts/pingRoutersScript');

router.get('/test-routers', async (req, res) => {
    try {
        const routers = await Router.query();
        res.json({ success: true, message: `La conexión exitosa, valor obtenido: ${routers}` });

    } catch (error) {
        res.status(500).json({ error: 'Error al conectarse : '+error });
    }

});

router.get('/test-ping', (req, res) => {
    realizarPingATodosLosRouters().then(() => {
        res.status(200).send('Ping iniciado a todos los routers');
    }).catch(error => {
        console.error('Error al realizar ping:', error);
        res.status(500).send('Error al iniciar el ping');
    });
});

const obtenerUltimasLatencias = require('../scripts/obtenerUltimasLatencias');

router.get('/ultimas-latencias', async (req, res) => {
    try {
        const latencias = await obtenerUltimasLatencias();
        res.json(latencias);
    } catch (error) {
        console.error(error);
        res.status(500).send('Error al recuperar las últimas latencias');
    }
});

const obtenerHistoricoLatencias = require('../scripts/obtenerHistoricoLatencias');

router.get('/historico-latencias/:routerId', async (req, res) => {
    const { routerId } = req.params;

    try {
        const latencias = await obtenerHistoricoLatencias(routerId);
        res.json(latencias);
    } catch (error) {
        console.error(error);
        res.status(500).send('Error al recuperar el histórico de latencias');
    }
});

module.exports = router;