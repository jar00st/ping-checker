const express = require('express');
const router = express.Router();
const db = require('../config/db');
const redisClient = require('../config/redis');

// Ruta para probar la conexión a MySQL
router.get('/test-db', (req, res) => {
    db.query('SELECT 1 + 1 AS solution', (error, results, fields) => {
        if (error) {
            return res.status(500).json({ error: 'Error al conectarse a la base de datos' });
        }
        res.json({ success: true, message: `La conexión a la base de datos es exitosa, 1 + 1 = ${results[0].solution}` });
    });
});

// Ruta para probar la conexión a Redis
router.get('/test-redis', async (req, res) => {
    try {
        await redisClient.set('test', 'Test successful');
        const value = await redisClient.get('test');
        res.json({ success: true, message: `La conexión a Redis es exitosa, valor obtenido: ${value}` });
    } catch (error) {
        res.status(500).json({ error: 'Error al conectarse a Redis' });
    }
});



module.exports = router;
