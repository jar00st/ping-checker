const { Model } = require('objection');
const knexConfig = require('../../knexfile');
const knex = require('knex')(knexConfig[process.env.NODE_ENV]);

Model.knex(knex);

class Router extends Model {
    static get tableName() {
        return 'routers';
    }

    static get idColumn() {
        return 'id';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['ip', 'hostname', 'name', 'identifier'],

            properties: {
                id: { type: 'integer' },
                ip: { type: 'string', minLength: 1, maxLength: 255 },
                hostname: { type: 'string', minLength: 1, maxLength: 255 },
                name: { type: 'string', minLength: 1, maxLength: 255 },
                identifier: { type: 'string', minLength: 1, maxLength: 255 }
            }
        };
    }

}

module.exports = Router;