const redis = require('redis');
require('dotenv').config();

let redisClient;

if (process.env.REDIS_URL) {
    redisClient = redis.createClient({
        url: process.env.REDIS_URL
    });

    redisClient.on('error', (err) => console.log('Redis Client Error', err));
    // No es necesario llamar a connect para esta versión
}

module.exports = redisClient;
