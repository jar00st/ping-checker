const express = require('express');
const bodyParser = require('body-parser');
const knexConfig = require('../knexfile');
const knex = require('knex')(knexConfig[process.env.NODE_ENV]);
const app = express();
const port = process.env.PORT || 8080;

// Middleware
app.use(bodyParser.json());

// Importar rutas
const testRoutes = require('./routes/tests');
const routerRouters = require('./routes/routers');

// Attach Knex instance to the Express app
app.set('knex', knex);

// Rutas
app.use('/api', testRoutes);
app.use('/api/routers', routerRouters);

//tasks
require('./tasks/pingRouters');

// Error handling middleware
app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});

// Start server
app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
