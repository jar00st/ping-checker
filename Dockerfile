FROM node:16-alpine

WORKDIR /usr/src/app

COPY package*.json ./


RUN apk add --update --no-cache python3 make g++ && ln -sf python3 /usr/bin/python

RUN npm install

COPY . .

EXPOSE 8080

CMD ["npx", "nodemon", "src/app.js"]

